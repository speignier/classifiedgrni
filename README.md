# ClassifiedGRNI: Gene Regulatory Network Data-driven Inference based on Classification Algorithms

This Python 3.7 package allows to infer Gene Regulatory Networks through several
classification-based data-driven methods. Pre-processing methods are also included.


### Tutorials:

Check the jupyter notebook tutorials located in the __tutorial__ folder

+ `Infer_dream5_E_coli_GRN_using_SVMs.ipynb` to infer GRNs using the SVM method (SVM classification)

### Authors:

For bug reports and feedback do not hesitate to contact the authors

+ [Sergio Peignier](https://sergiopeignier.github.io/): sergio.peignier AT insa-lyon.fr
+ Pauline Schmitt: pauline.schmitt AT insa-lyon.fr

### Maintainer:

+ [Sergio Peignier](https://sergiopeignier.github.io/): sergio.peignier AT insa-lyon.fr
