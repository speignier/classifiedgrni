"""
ClassifiedGRNI

This package allows to infer Gene Regulatory Networks through several
classification-based data-driven methods.
Pre-processing methods are also included.

"""
__author__ = "Sergio Peignier and Pauline Schmitt"
__copyright__ = "Copyright 2019, The ClassifiedGRNI Project"
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Sergio Peignier"
__email__ = "sergio.peignier@insa-lyon.fr"
__status__ = "pre-alpha"
